/**
 * @file UserFaceEngine.h
 * @brief Handle interface with the user.
 * @author Tomas Jakubik
 * @date Jun 29, 2021
 */

#ifndef USERFACEENGINE_H_
#define USERFACEENGINE_H_

#include <packet.h> //Definition of sensor config and packet format
#include <stdint.h>
#include <UserFaceConfig.h> //Definitions matching userface to platform

/**
 * @brief To be inherited into a one user command.
 */
class UserFaceCommand;

/**
 * @brief Input text input from user and change sensor config.
 */
class UserFaceEngine
{
    friend UserFaceCommand;

public:
    /**
     * @brief User changeable configuration.
     */
    struct Config
    {
        static const constexpr int CONFIGS_N = 10;   ///< Number of sensors configured

        /**
         * @brief Control structure for one sensor.
         */
        struct SensorControl
        {
            bool changed = false;   ///< Marked if the config changes
            bool sent = false;  ///< Marked when new config is sent
            uint8_t seq = 0;    ///< Marks last received sequence number
            control_t config =  ///< Configuration and packet to be sent
            {                                                                \
                .proto = { .header = HEADER_CONTROL, .id = 0, .seq = 0 },    \
                .bias                = 1,                                    \
                .int_z               = 1,                                    \
                .r_load              = 0,                                    \
                .tia_gain            = 7,                                    \
                .bias_positive       = 0,                                    \
                .ref_source_external = 0,                                    \
                .secondly            = 1,                                    \
                .multi_f             = 0,                                    \
                .threshold_enable    = 0,                                    \
                .threshold           = 20,                                   \
            };
        };

        SensorControl active_control;   ///< Sensor config being actively changed
        int active_control_i;           ///< Index of active config in user_face_controls
        SensorControl sensor_controls[CONFIGS_N];   ///< Configurations for sensors

        /**
         * @brief Mark config IDs.
         */
        Config() : active_control_i(0)
        {
            for(int i = 0; i < CONFIGS_N; i++)
            {
                sensor_controls[i].config.proto.id = i+1;
            }
        }
    };
    static Config user_config;  ///< User changeable configuration.

private:
    static UserFaceCommand *commands;   ///< List of available commands

public:
    /**
     * @brief Print commands and their info.
     */
    static void print_help();

    /**
     * @brief Input of text from user
     * @param data modifiable buffer
     * @param len number of characters in in
     */
    static void in(char* data, int len);
};

/**
 * @brief To be inherited into a one user command.
 */
class UserFaceCommand
{
    /**
     * @brief Reference to a next item in a linked list.
     */
    UserFaceCommand *next;

protected:
    /**
     * @brief Reference of the user_config for the command handler.
     */
    UserFaceEngine::Config &user_config;

public:
    UserFaceCommand() :
        user_config(UserFaceEngine::user_config)
    {
        //Store itself at the beginning of the list
        next = UserFaceEngine::commands;
        UserFaceEngine::commands = this;
    }

    virtual ~UserFaceCommand(){}

    /**
     * @brief Pointer to next command in list.
     * @return pointer or nullptr
     */
    UserFaceCommand* get_next()
    {
        return next;
    }

    /**
     * @brief Return command name as typed by user.
     * @return constant null-terminated string
     */
    virtual const char* name() = 0;

    /**
     * @brief Return brief info about this command for help.
     * @return constant null-terminated string
     */
    virtual const char* info() = 0;

    /**
     * @brief Command handler.
     * @param arg null-terminated command arguments
     */
    virtual void command(const char *arg) = 0;
};

#endif /* USERFACEENGINE_H_ */
