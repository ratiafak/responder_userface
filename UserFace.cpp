/**
 * @file UserFace.cpp
 * @brief Definition of an interface with the user.
 * @author Tomas Jakubik
 * @date Jan 8, 2021
 */

#include <UserFaceEngine.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>

/**
 * @brief Print help about other commands.
 */
class HelpCommand: public UserFaceCommand
{
    const char* name()
    {
        return "help";
    }

    const char* info()
    {
        return " - Show this";
    }

    ///@param arg ignored
    void command(const char *arg)
    {
        USERFACE_OUT("Help for ");
        USERFACE_OUT(USERFACE_FW_NAME);
        USERFACE_OUT(USERFACE_NEWLINE " version: ");
        USERFACE_OUT(USERFACE_FW_VER);
        USERFACE_OUT(USERFACE_NEWLINE "--------------------------------" USERFACE_NEWLINE);

        UserFaceEngine::print_help();

        USERFACE_OUT(USERFACE_NEWLINE "Data from sensors are reported as csv:" USERFACE_NEWLINE);
        USERFACE_OUT("\"Data\", sensor id, timestamp [s], temperature [°C], relative humidity [%], LMP WE current [nA]" USERFACE_NEWLINE);
        USERFACE_OUT("Timestamps can be repeated if RF packet is not delivered on first try." USERFACE_NEWLINE);
        USERFACE_OUT("Fix by: \"[~, unique_indexes, ~] = unique(data(:,2)); unique_data = data(unique_indexes,:);\"." USERFACE_NEWLINE);

        USERFACE_OUT(USERFACE_NEWLINE "Extra value can come if threshold is reached (not a csv):" USERFACE_NEWLINE);
        USERFACE_OUT("@ {tm} s - Extra from {id}, nearest timestamp {ts}, extra seq {s}, value {i}." USERFACE_NEWLINE);
        USERFACE_OUT("  tm is time when packet was received." USERFACE_NEWLINE);
        USERFACE_OUT("  id is sensor id." USERFACE_NEWLINE);
        USERFACE_OUT("  ts is approximate nearest timestamp [s], the measurement is not aligned to seconds." USERFACE_NEWLINE);
        USERFACE_OUT("  s  is sequence to detect repeated or individual values." USERFACE_NEWLINE);
        USERFACE_OUT("  i  is LMP WE current [nA]." USERFACE_NEWLINE);

        USERFACE_OUT(USERFACE_NEWLINE);
    }
} help_command;

/**
 * @brief List current configuration.
 */
class ListCommand: public UserFaceCommand
{
    const char* name()
    {
        return "list";
    }

    const char* info()
    {
        return " - List current configuration";
    }

    ///@param arg ignored
    void command(const char *arg)
    {
        static const constexpr int BUF_SIZE = 128;
        char buf[BUF_SIZE];
        int i;  ///< Iterate over sensors

        USERFACE_OUT("Current configuration" USERFACE_NEWLINE);

        for (i = 0; i < user_config.CONFIGS_N; i++)
        {
            snprintf(buf, BUF_SIZE, " Sensor %i:" USERFACE_NEWLINE, user_config.sensor_controls[i].config.proto.id);
            USERFACE_OUT(buf);
            snprintf(buf, BUF_SIZE, "   tia_gain %x" USERFACE_NEWLINE, user_config.sensor_controls[i].config.tia_gain);
            USERFACE_OUT(buf);
            snprintf(buf, BUF_SIZE, "     r_load %x" USERFACE_NEWLINE, user_config.sensor_controls[i].config.r_load);
            USERFACE_OUT(buf);
            snprintf(buf, BUF_SIZE, "    ref_ext %i" USERFACE_NEWLINE, user_config.sensor_controls[i].config.ref_source_external);
            USERFACE_OUT(buf);
            snprintf(buf, BUF_SIZE, "      int_z %x" USERFACE_NEWLINE, user_config.sensor_controls[i].config.int_z);
            USERFACE_OUT(buf);
            snprintf(buf, BUF_SIZE, "   bias_pos %i" USERFACE_NEWLINE, user_config.sensor_controls[i].config.bias_positive);
            USERFACE_OUT(buf);
            snprintf(buf, BUF_SIZE, "       bias %x" USERFACE_NEWLINE, user_config.sensor_controls[i].config.bias);
            USERFACE_OUT(buf);
            snprintf(buf, BUF_SIZE, "   secondly %i" USERFACE_NEWLINE, user_config.sensor_controls[i].config.secondly);
            USERFACE_OUT(buf);
            snprintf(buf, BUF_SIZE, "    multi_f %i" USERFACE_NEWLINE, user_config.sensor_controls[i].config.multi_f);
            USERFACE_OUT(buf);
            if (user_config.sensor_controls[i].config.threshold_enable)
            {
                snprintf(buf, BUF_SIZE, "  threshold %i nA" USERFACE_NEWLINE, user_config.sensor_controls[i].config.threshold);
            }
            else
            {
                snprintf(buf, BUF_SIZE, "  threshold disabled" USERFACE_NEWLINE);
            }
            USERFACE_OUT(buf);
            USERFACE_OUT(USERFACE_NEWLINE);
        }
    }
} list_command;

/**
 * @brief Echo given text.
 */
class EchoCommand: public UserFaceCommand
{
    const char* name()
    {
        return "echo";
    }

    const char* info()
    {
        return " - Echo given text";
    }

    ///@param arg text to echo
    void command(const char *arg)
    {
        if (arg == nullptr)
        {
            USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
            return;
        }

        unsigned int now = USERFACE_TIME_MS();
        static const constexpr int BUF_SIZE = 32;
        char buf[BUF_SIZE];
        snprintf(buf, BUF_SIZE, "@ %7u.%03u s - ", now / 1000, now % 1000);
        USERFACE_OUT(buf);
        USERFACE_OUT(arg);
        USERFACE_OUT(USERFACE_NEWLINE);
    }
} echo_command;

/**
 * @brief Command to start configuration.
 */
class ConfigCommand: public UserFaceCommand
{
    const char* name()
    {
        return "config";
    }

    const char* info()
    {
        return " x - Start configuration of sensor x";
    }

    ///@param arg one expected argument of sensor id
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            USERFACE_OUT("Already editing" USERFACE_NEWLINE);
        }
        else
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            user_config.active_control_i = *arg - '1';
            if ((user_config.active_control_i < 0) || (user_config.active_control_i >= user_config.CONFIGS_N))
            {
                USERFACE_OUT("Sensor number not known" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control = user_config.sensor_controls[user_config.active_control_i];
                user_config.active_control.changed = true;

                static const constexpr int BUF_SIZE = 32;
                char buf[BUF_SIZE];
                snprintf(buf, BUF_SIZE, "Editing sensor %i" USERFACE_NEWLINE, user_config.active_control.config.proto.id);
                USERFACE_OUT(buf);
            }
        }
    }
} config_command;

/**
 * @brief Command to finish configuration and write to sensor.
 */
class FinishCommand: public UserFaceCommand
{
    const char* name()
    {
        return "finish";
    }

    const char* info()
    {
        return " - Finish configuration of sensor x and apply on next message";
    }

    ///@param arg ignored
    virtual void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            static const constexpr int BUF_SIZE = 128;
            char buf[BUF_SIZE];
            snprintf(buf, BUF_SIZE, "Sensor %i config stored, will be applied on next communication" USERFACE_NEWLINE,
                     user_config.active_control.config.proto.id);
            USERFACE_OUT(buf);

            user_config.sensor_controls[user_config.active_control_i].config = user_config.active_control.config;
            user_config.sensor_controls[user_config.active_control_i].sent = false;
            user_config.sensor_controls[user_config.active_control_i].changed = true;
            user_config.active_control.changed = false;
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} finish_command;

/**
 * @brief Throw away currently edited config.
 */
class DiscardCommand: public UserFaceCommand
{
    const char* name()
    {
        return "discard";
    }

    const char* info()
    {
        return " - Throw away currently edited config";
    }

    ///@param arg ignored
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            USERFACE_OUT("Discarded" USERFACE_NEWLINE);
            user_config.active_control.changed = false;
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} discard_command;

/**
 * @brief Set TIA gain.
 */
class TiaGainCommand: public UserFaceCommand
{
    const char* name()
    {
        return "tia_gain";
    }

    const char* info()
    {
        return " x - TIA feedback resistance selection" USERFACE_NEWLINE
            "    0 - External resistance" USERFACE_NEWLINE
            "    1 -   2.75 kOhm" USERFACE_NEWLINE
            "    2 -   3.5  kOhm" USERFACE_NEWLINE
            "    3 -   7    kOhm" USERFACE_NEWLINE
            "    4 -  14    kOhm" USERFACE_NEWLINE
            "    5 -  35    kOhm" USERFACE_NEWLINE
            "    6 - 120    kOhm" USERFACE_NEWLINE
            "    7 - 350    kOhm";
    }

    ///@param arg 0~7 one gain option
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            int select = *arg - '0';
            if ((select < 0) || (select > 7))
            {
                USERFACE_OUT("Wrong parameter" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control.config.tia_gain = select;
            }
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} tia_gain_command;

/**
 * @brief Set R load.
 */
class RLoadCommand: public UserFaceCommand
{
    const char* name()
    {
        return "r_load";
    }

    const char* info()
    {
        return " x - R Load selection" USERFACE_NEWLINE
            "    0 -  10 Ohm" USERFACE_NEWLINE
            "    1 -  33 Ohm" USERFACE_NEWLINE
            "    2 -  50 Ohm" USERFACE_NEWLINE
            "    3 - 100 Ohm";
    }

    ///@param arg 0~3 one r option
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            int select = *arg - '0';
            if ((select < 0) || (select > 3))
            {
                USERFACE_OUT("Wrong parameter" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control.config.r_load = select;
            }
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} r_load_command;

/**
 * @brief Enable external reference.
 */
class RefExtCommand: public UserFaceCommand
{
    const char* name()
    {
        return "ref_ext";
    }

    const char* info()
    {
        return " 0/1 - Enable VREF as source, VDD otherwise";
    }

    ///@param arg 1 for VREF or 0 for VDD
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            int en = *arg - '0';
            if ((en < 0) || (en > 1))
            {
                USERFACE_OUT("Wrong parameter" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control.config.ref_source_external = en;
            }
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} ref_ext_command;

/**
 * @brief Set internal zero level.
 */
class IntZCommand: public UserFaceCommand
{
    const char* name()
    {
        return "int_z";
    }

    const char* info()
    {
        return " x - Internal zero selection as percentage of source reference" USERFACE_NEWLINE
            "    0 - 20%" USERFACE_NEWLINE
            "    1 - 50%" USERFACE_NEWLINE
            "    2 - 67%" USERFACE_NEWLINE
            "    3 - Internal zero circuitry bypassed";
    }

    ///@param arg 0~3 one level option
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            int select = *arg - '0';
            if ((select < 0) || (select > 3))
            {
                USERFACE_OUT("Wrong parameter" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control.config.int_z = select;
            }
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} int_z_command;

/**
 * @brief Set positive or negative bias.
 */
class BiasPosCommand: public UserFaceCommand
{
    const char* name()
    {
        return "bias_pos";
    }

    const char* info()
    {
        return " 0/1 - Enable positive bias";
    }

    ///@param arg 1 for positive or 0 for negative
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            int en = *arg - '0';
            if ((en < 0) || (en > 1))
            {
                USERFACE_OUT("Wrong parameter" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control.config.bias_positive = en;
            }
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} bias_pos_command;

/**
 * @brief Set bias percentage.
 */
class BiasCommand: public UserFaceCommand
{
    const char* name()
    {
        return "bias";
    }

    const char* info()
    {
        return " x - Bias as percentage of source reference" USERFACE_NEWLINE
            "    0 -  0%" USERFACE_NEWLINE
            "    1 -  1%" USERFACE_NEWLINE
            "    2 -  2%" USERFACE_NEWLINE
            "    3 -  4%" USERFACE_NEWLINE
            "    4 -  6%" USERFACE_NEWLINE
            "    5 -  8%" USERFACE_NEWLINE
            "    6 - 10%" USERFACE_NEWLINE
            "    7 - 12%" USERFACE_NEWLINE
            "    8 - 14%" USERFACE_NEWLINE
            "    9 - 16%" USERFACE_NEWLINE
            "    a - 18%" USERFACE_NEWLINE
            "    b - 20%" USERFACE_NEWLINE
            "    c - 22%" USERFACE_NEWLINE
            "    d - 24%";
    }

    ///@param arg 0~d one bias option
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            int select;
            if (*arg > 'a')
            {
                select = *arg - 'a' + 10;
            }
            else
            {
                select = *arg - '0';
            }
            if ((select < 0) || (select > 13))
            {
                USERFACE_OUT("Wrong parameter" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control.config.bias = select;
            }
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} bias_command;

/**
 * @brief Set secondly measurements.
 */
class SecondlyCommand: public UserFaceCommand
{
    const char* name()
    {
        return "secondly";
    }

    const char* info()
    {
        return " 0/1 - Enable secondly measurements instead of minutely";
    }

    ///@param arg 1 for secondly or 0 for minutely
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            int en = *arg - '0';
            if ((en < 0) || (en > 1))
            {
                USERFACE_OUT("Wrong parameter" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control.config.secondly = en;
            }
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} secondly_command;

/**
 * @brief Set multiple frequencies for sensor.
 */
class MultiFCommand: public UserFaceCommand
{
    const char* name()
    {
        return "multi_f";
    }

    const char* info()
    {
        return " 0/1 - Enable transmitting on random frequency";
    }

    ///@param arg 1 to enable switching frequencies or 0 to disable
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            int en = *arg - '0';
            if ((en < 0) || (en > 1))
            {
                USERFACE_OUT("Wrong parameter" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control.config.multi_f = en;
            }
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} multi_f_command;

/**
 * @brief Set current threshold.
 */
class ThresholdCommand: public UserFaceCommand
{
    const char* name()
    {
        return "threshold";
    }

    const char* info()
    {
        return " 00000 - Enable current threshold [nA]";
    }

    ///@param threshold current [nA]
    void command(const char *arg)
    {

        if (user_config.active_control.changed)
        {
            if (arg == nullptr)
            {
                USERFACE_OUT("Needs parameter" USERFACE_NEWLINE);
                return;
            }

            char *end = nullptr;
            int current = strtol(arg, &end, 10);
            if ((end == arg) || (end == nullptr))
            {
                USERFACE_OUT("Wrong parameter" USERFACE_NEWLINE);
            }
            else
            {
                user_config.active_control.config.threshold = current;
                user_config.active_control.config.threshold_enable = 1;
            }
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} threshold_command;

/**
 * @brief Disable current threshold.
 */
class ThresholdDisableCommand: public UserFaceCommand
{
    const char* name()
    {
        return "threshold_disable";
    }

    const char* info()
    {
        return " - Disable current threshold";
    }

    ///@param arg ignored
    void command(const char *arg)
    {
        if (user_config.active_control.changed)
        {
            user_config.active_control.config.threshold_enable = 0;
        }
        else
        {
            USERFACE_OUT("No config open" USERFACE_NEWLINE);
        }
    }
} threshold_disable_command;

