/**
 * @file UserFaceEngine.cpp
 * @brief Handle interface with the user.
 * @author Tomas Jakubik
 * @date Jun 29, 2021
 */

#include <UserFaceEngine.h>
#include <cstring>

UserFaceEngine::Config UserFaceEngine::user_config;  ///< User changeable configuration.
UserFaceCommand *UserFaceEngine::commands = nullptr;   ///< List of available commands

/**
 * @brief Print commands and their info.
 */
void UserFaceEngine::print_help()
{
    USERFACE_OUT("Available config:" USERFACE_NEWLINE);
    UserFaceCommand* command = commands;
    while (command != nullptr)
    {
        USERFACE_OUT("  ");
        USERFACE_OUT(command->name());
        USERFACE_OUT(command->info());
        USERFACE_OUT(USERFACE_NEWLINE);
        command = command->get_next();
    }
}

/**
 * @brief Input of text from user
 * @param data modifiable buffer
 * @param len number of characters in in
 */
void UserFaceEngine::in(char* data, int len)
{
    //Separate command from arguments
    char* arg = static_cast<char*>(memchr(data, ' ', len));
    if (arg != nullptr)
    {
        *arg = '\0';
        arg++;
    }

    //Go through commands
    UserFaceCommand* command = commands;
    while (command != nullptr)
    {
        int cmd_len = strlen(command->name());
        if ((len >= cmd_len) && (strncmp(command->name(), data, len) == 0))
        {
            command->command(arg); //Call command callback
            return; //Finished
        }
        command = command->get_next();
    }
    USERFACE_OUT("Unknown command!" USERFACE_NEWLINE);
}


